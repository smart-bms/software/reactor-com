## The Reactor COM protocol (RCom)
### Project goals
RCom is a simple lightweight and scalable protocol to designed to communicate
with embedded devices. It consists of common, template code and a generator
that reads JSON recipe to generate the proper protocol API for messages
serialization.
By design both, the common code and the generated code is being split into two
layers:
- the core layer, that's the raw protocol implementation with simple C API
that aims to be portable from simple AVR microcontrollers up to mobile platforms
and desktop computers. The only dependency is libc so it should be able to be
compiled on every platform with just C compiler. The generated code contains
an algorithm for converting messages into blocks of data that are passed to
serializator, which makes of them a stream of data. The common code contains
a simple header-data-checksum serializator that can be used to transfer data
over any serial port. However it can be replaced with more advanced serializator
to add, for example, addressing or other features.
Once the message is serialized, it's passed further into the stream by one or
more invocation of callback function that takes an array of bytes and optional
user data pointer.
In the other direction when the data is received, it has to be passed to feed
function that uses deserializator to read message and invoke proper receiver
function with the same arguments as it was sent.
On embedded platforms, the write callback and feed function may be
connected, for example, to UART stream of other serial transmission layer.
- The glib layer which is a high-level wrapper over core layer with some
implementation details hidden in place of higher abstraction. It wraps both the
common and generated code into GObject classes which can be then used with 
GObject Introspection to use it in other programming languages like Vala or
Python. It can be also easier to use with C on platforms where glib dependency
is available.
Additionally glib layer provides several implementations of communication
endpoint, for example rcom-termios module implements tty target based on
low-level UNIX termios API to configure serial port. I plan to add soon
bluez target to communicate over bluetooth and socket target to allow
communication with embedded code emulated on the host machine.
size.

### Codegen
RCom Codegen is a tool compiled for the host machine during build. It's the
final protocol code generator that reads the JSON declarations. It generates
both the core code and glib wrapper.

### JSON declaration
Declaration file contains a root object with protocol properties, enumeration
and flag constants, custom structures and messages.
The formal syntax is defined in codegen/tree.json file which is a mkit-declgen
declaration.  
TODO write README about mkit project and add reference here

### BCom protocol
BCom is a specialized protocol based on RCom that is used to communicate with
Reactor BMS board. It could be a small separate project but currently it drives
the RCom architecture so it's in bcom/ subdirectory.
The main point is bcom.json protocol declaration that's used by RCom to generate
BCom's code. Additionally it implements bcom-curve structure that represents a
non-linear X -> Y numeric mapping as a curve of up to 9 steps.
In bcom.json the curve is declared as an extern structure so it can be properly
handled by the code generator.

### Examples
examples/ directory contains some example C and Python usage of RCom/BCom like
virtual ping between two ports or pinging a real device with RCom.Termios target.
I will put there more example code soon.

### Building
The best to use with Meson build system.  
It can be built with CMake as well but it requires to build and install mkit
first because it doesn't support CMake build and I
don't plan it will ever do.  
To compile RCom with CMake, clone mkit (https://gitlab.com/mimaz/mkit) to
another directory first and run there: meson build && ninja -C build install  
Then it should be possible to run cmake with RCom as usually. At least I do so
with my Zephyr OS project.
