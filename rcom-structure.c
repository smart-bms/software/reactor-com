#include "rcom-glib.h"

typedef struct {
    gpointer raw_pointer;
    gboolean raw_copy;
    guint raw_size;
} RComStructurePrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (RComStructure, rcom_structure,
				     G_TYPE_OBJECT);

enum
{
    PROP_0,
    PROP_RAW_POINTER,
    PROP_RAW_COPY,
    PROP_RAW_SIZE,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void
set_property (GObject *gobj, guint id,
	      const GValue *value, GParamSpec *spec)
{
    RComStructure *self = RCOM_STRUCTURE (gobj);
    RComStructurePrivate *p = rcom_structure_get_instance_private (self);

    switch (id) {
    case PROP_RAW_POINTER:
	p->raw_pointer = g_value_get_pointer (value);
	break;

    case PROP_RAW_COPY:
	p->raw_copy = g_value_get_boolean (value);
	break;

    case PROP_RAW_SIZE:
	p->raw_size = g_value_get_uint (value);
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
get_property (GObject *gobj, guint id,
	      GValue *value, GParamSpec *spec)
{
    RComStructure *self = RCOM_STRUCTURE (gobj);

    switch (id) {
    case PROP_RAW_POINTER:
	g_value_set_pointer (value, rcom_structure_get_raw (self));
	break;

    case PROP_RAW_COPY:
	g_value_set_boolean (value, rcom_structure_get_raw_copy (self));
	break;

    case PROP_RAW_SIZE:
	g_value_set_uint (value, rcom_structure_get_raw_size (self));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
constructed (GObject *gobj)
{
    G_OBJECT_CLASS (rcom_structure_parent_class)->constructed (gobj);

    RComStructure *self = RCOM_STRUCTURE (gobj);
    RComStructurePrivate *p = rcom_structure_get_instance_private (self);

    if (!p->raw_pointer) {
	g_message ("raw alloc %u", p->raw_size);
	g_assert (p->raw_size);
	p->raw_pointer = g_malloc0 (p->raw_size);
	p->raw_copy = TRUE;
    } else if (p->raw_copy) {
	g_assert (p->raw_size);
	p->raw_pointer = g_memdup2 (p->raw_pointer, p->raw_size);
    }
}

static void
finalize (GObject *gobj)
{
    RComStructure *self = RCOM_STRUCTURE (gobj);
    RComStructurePrivate *p = rcom_structure_get_instance_private (self);

    if (p->raw_copy) {
	g_free (p->raw_pointer);
    }

    G_OBJECT_CLASS (rcom_structure_parent_class)->finalize (gobj);
}

static void
rcom_structure_init (RComStructure *self)
{
}

static void
rcom_structure_class_init (RComStructureClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->set_property = set_property;
    gcls->get_property = get_property;
    gcls->finalize = finalize;
    gcls->constructed = constructed;

    props[PROP_RAW_POINTER] =
	g_param_spec_pointer ("raw-pointer",
			      "raw-pointer",
			      "raw-pointer",
			      G_PARAM_READWRITE |
			      G_PARAM_CONSTRUCT_ONLY |
			      G_PARAM_STATIC_STRINGS);

    props[PROP_RAW_COPY] =
	g_param_spec_boolean ("raw-copy",
			      "raw-copy",
			      "raw-copy",
			      FALSE,
			      G_PARAM_READWRITE |
			      G_PARAM_CONSTRUCT_ONLY |
			      G_PARAM_STATIC_STRINGS);

    props[PROP_RAW_SIZE] =
	g_param_spec_uint ("raw-size",
			   "raw-size",
			   "raw-size",
			   0, G_MAXUINT32, 0,
			   G_PARAM_READWRITE |
			   G_PARAM_CONSTRUCT_ONLY |
			   G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (gcls, N_PROPS,
				       props);
}

gpointer
rcom_structure_construct (GType type, gpointer raw_pointer,
			  gboolean raw_copy, gsize raw_size)
{
    g_return_val_if_fail (g_type_is_a (type, RCOM_TYPE_STRUCTURE), NULL);

    return g_object_new (type,
			 "raw-pointer", raw_pointer,
			 "raw-copy", raw_copy,
			 "raw-size", raw_size,
			 NULL);
}

gpointer
rcom_structure_get_raw (RComStructure *self)
{
    RComStructurePrivate *p = rcom_structure_get_instance_private (self);

    return p->raw_pointer;
}

gboolean
rcom_structure_get_raw_copy (RComStructure *self)
{
    RComStructurePrivate *p = rcom_structure_get_instance_private (self);

    return p->raw_copy;
}

guint
rcom_structure_get_raw_size (RComStructure *self)
{
    RComStructurePrivate *p = rcom_structure_get_instance_private (self);

    return p->raw_size;
}

gboolean
rcom_structure_equal (RComStructure *self, RComStructure *other)
{
    if (self == other) {
	return TRUE;
    }

    if (!self || !other) {
	return FALSE;
    }

    g_return_val_if_fail (rcom_structure_get_raw_size (self) ==
			  rcom_structure_get_raw_size (other), FALSE);

    return memcmp (rcom_structure_get_raw (self),
		   rcom_structure_get_raw (other),
		   rcom_structure_get_raw_size (self));
}

void
rcom_structure_copy (RComStructure *self, RComStructure *dest)
{
    g_autofree GParamSpec **props = NULL;
    g_auto (GValue) src_value = G_VALUE_INIT;
    g_auto (GValue) dst_value = G_VALUE_INIT;
    guint num_props, i;
    gboolean equal;

    if (self == dest) {
	return;
    }

    g_return_if_fail (dest);
    g_return_if_fail (!self || (G_OBJECT_GET_CLASS (self) ==
		      G_OBJECT_GET_CLASS (dest)));
    g_return_if_fail (!self || (rcom_structure_get_raw_size (self) ==
		      rcom_structure_get_raw_size (dest)));

    if (self) {
	memcpy (rcom_structure_get_raw (dest),
		rcom_structure_get_raw (self),
		rcom_structure_get_raw_size (self));
    }

    props = g_object_class_list_properties (self ?
					    G_OBJECT_GET_CLASS (self) :
					    G_OBJECT_GET_CLASS (dest),
					    &num_props);

    for (i = 0; i < num_props; i++) {
	if (rcom_structure_is_reserved (props[i]->name)) {
	    continue;
	}

	g_value_unset (&src_value);
	g_value_unset (&dst_value);

	g_object_get_property (G_OBJECT (dest), props[i]->name, &dst_value);

	if (self) {
	    g_object_get_property (G_OBJECT (self), props[i]->name, &src_value);
	} else {
	    g_value_init (&src_value, G_VALUE_TYPE (&dst_value));
	}

	if (G_VALUE_HOLDS (&src_value, RCOM_TYPE_STRUCTURE)) {
	    equal = rcom_structure_equal (g_value_get_object (&src_value),
					  g_value_get_object (&dst_value));
	} else {
	    equal = memcmp (&src_value, &dst_value, sizeof (GValue)) == 0;
	}

	if (!equal) {
	    g_object_notify_by_pspec (G_OBJECT (dest), props[i]);
	}
    }
}

gboolean
rcom_structure_is_reserved (const gchar *prop_name)
{
    static const gchar *skip_names[] = {
	"raw-pointer",
	"raw-copy",
	"raw-size",
    };
    guint i;

    for (i = 0; i < G_N_ELEMENTS (skip_names); i++) {
	if (g_strcmp0 (prop_name, skip_names[i]) == 0) {
	    return TRUE;
	}
    }

    return FALSE;
}
