# Copyright (c) 2020,2021 Reactor Energy
# Mieszko Mazurek <mimaz@gmx.com>

project('reactor-com', 'c', 'vala', version: '1.0')

pkg = import('pkgconfig')
gnome = import('gnome')
mkit_dep = dependency('mkit', default_options: 'gir=false')
gobject_dep = dependency('gobject-2.0')
gio_dep = dependency('gio-2.0')

rcom_lib = library(
  'rcom-core',
  'rcom-core.c',
  install: true,
)

rcom_dep = declare_dependency(
  link_with: rcom_lib,
  include_directories: '.',
)

rcom_glib_lib = shared_library(
  'rcom-glib',
  'rcom-error.c',
  'rcom-file.c',
  'rcom-port.c',
  'rcom-serial.c',
  'rcom-structure.c',
  'rcom-target.c',
  'rcom-termios.c',
  dependencies: [
    rcom_dep,
    mkit_dep,
    gio_dep,
  ],
  install: true,
)

rcom_glib_dep = declare_dependency(
  link_with: rcom_glib_lib,
  dependencies: [
    rcom_dep,
    gobject_dep,
  ],
)

if get_option('gir')
  rcom_gir = gnome.generate_gir(
    rcom_glib_lib,
    sources: [
      'rcom-error.c',
      'rcom-error.h',
      'rcom-file.c',
      'rcom-file.h',
      'rcom-port.c',
      'rcom-port.h',
      'rcom-serial.c',
      'rcom-serial.h',
      'rcom-structure.c',
      'rcom-structure.h',
      'rcom-target.c',
      'rcom-target.h',
      'rcom-termios.c',
      'rcom-termios.h',
    ],
    namespace: 'RCom',
    identifier_prefix: 'RCom',
    symbol_prefix: 'rcom',
    nsversion: '1.0',
    header: 'rcom-glib.h',
    includes: 'GObject-2.0',
    install: true,
  )

  rcom_typelib_path = meson.current_build_dir()

  rcom_glib_dep = declare_dependency(
    dependencies: rcom_glib_dep,
    sources: rcom_gir,
  )

  if get_option('vapi')
    rcom_vapi = gnome.generate_vapi(
      'RCom-1.0',
      packages: 'gobject-2.0',
      sources: rcom_gir[0],
      install: true,
    )
  endif
endif

meson.override_dependency('rcom', rcom_dep)
meson.override_dependency('rcom-glib', rcom_glib_dep)

subdir('codegen')
subdir('bcom')
subdir('examples')
