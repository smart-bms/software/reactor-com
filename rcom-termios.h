#pragma once

#include "rcom-file.h"

G_BEGIN_DECLS

#define RCOM_TYPE_TERMIOS (rcom_termios_get_type ())

G_DECLARE_DERIVABLE_TYPE (RComTermios, rcom_termios,
			  RCOM, TERMIOS, RComFile);

struct _RComTermiosClass
{
    RComFileClass parent_class;
};

RComTermios    *rcom_termios_new	(const gchar *device);
void		rcom_termios_set_device	(RComTermios *self,
					 const gchar *device);
const gchar    *rcom_termios_get_device	(RComTermios *self);

G_END_DECLS
