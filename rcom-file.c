#include "rcom-file.h"
#include "rcom-target.h"

#include <mkit.h>

typedef struct {
    MkString *inpath;
    MkString *outpath;

    GFile *infile;
    GFile *outfile;

    GInputStream *istream;
    GIOStream *ostream;

    GFileMonitor *inmonitor;
    GFileMonitor *outmonitor;

    GIOChannel *inchannel;
    guint insource;
} RComFilePrivate;

static guint sig_io_error;

static void
emit_error (RComFile *self, GError *error)
{
    g_signal_emit (self, sig_io_error, 0, error);
}

static gint
write_data (RComTarget *target, guint size, gconstpointer data)
{
    g_autoptr (GError) error = NULL;
    RComFile *self = RCOM_FILE (target);
    GOutputStream *ostream = rcom_file_get_output (self, &error);
    gssize written = -1;

    if (ostream) {
	written = g_output_stream_write (ostream, data, size, NULL, &error);
    }

    if (error) {
	emit_error (self, error);
    }

    return written;
}

static void
target_iface_init (RComTargetInterface *iface)
{
    iface->write_data = write_data;
}

G_DEFINE_TYPE_WITH_CODE (RComFile, rcom_file, G_TYPE_OBJECT,
			 G_IMPLEMENT_INTERFACE (RCOM_TYPE_TARGET,
						target_iface_init)
			 G_ADD_PRIVATE (RComFile));

enum
{
    PROP_0,
    PROP_INPATH,
    PROP_OUTPATH,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void
set_property (GObject *gobj, guint id, const GValue *value, GParamSpec *spec)
{
    RComFile *self = RCOM_FILE (gobj);

    switch (id) {
    case PROP_INPATH:
	rcom_file_set_inpath (self, g_value_get_string (value));
	break;

    case PROP_OUTPATH:
	rcom_file_set_outpath (self, g_value_get_string (value));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
get_property (GObject *gobj, guint id, GValue *value, GParamSpec *spec)
{
    RComFile *self = RCOM_FILE (gobj);

    switch (id) {
    case PROP_INPATH:
	g_value_set_string (value, rcom_file_get_inpath (self));
	break;

    case PROP_OUTPATH:
	g_value_set_string (value, rcom_file_get_outpath (self));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
clear_inchannel (RComFile *self)
{
    RComFilePrivate *p = rcom_file_get_instance_private (self);

    if (p->insource) {
	g_source_remove (p->insource);
	p->insource = 0;
    }

    g_clear_pointer (&p->inchannel, g_io_channel_unref);
}

static void
finalize (GObject *gobj)
{
    RComFile *self = RCOM_FILE (gobj);
    RComFilePrivate *p = rcom_file_get_instance_private (self);

    clear_inchannel (self);

    g_clear_object (&p->infile);
    g_clear_object (&p->outfile);

    g_clear_object (&p->istream);
    g_clear_object (&p->ostream);

    g_clear_object (&p->inmonitor);
    g_clear_object (&p->outmonitor);

    mk_clear_string (&p->inpath);
    mk_clear_string (&p->outpath);

    G_OBJECT_CLASS (rcom_file_parent_class)->finalize (gobj);
}

static void
rcom_file_init (RComFile *self)
{

}

static void
mkinput (RComFile *self, GFile *file, GError **error)
{}

static void
mkoutput (RComFile *self, GFile *file, GError **error)
{}

static void
rcom_file_class_init (RComFileClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    cls->mkinput = mkinput;
    cls->mkoutput = mkoutput;
    gcls->set_property = set_property;
    gcls->get_property = get_property;
    gcls->finalize = finalize;

    sig_io_error = g_signal_new ("io-error",
				 RCOM_TYPE_FILE,
				 G_SIGNAL_RUN_LAST,
				 0, NULL, NULL, NULL,
				 G_TYPE_NONE, 1, G_TYPE_ERROR);

    props[PROP_INPATH] = g_param_spec_string ("inpath",
					      "inpath",
					      "inpath",
					      NULL,
					      G_PARAM_READWRITE |
					      G_PARAM_CONSTRUCT |
					      G_PARAM_STATIC_STRINGS |
					      G_PARAM_EXPLICIT_NOTIFY);

    props[PROP_OUTPATH] = g_param_spec_string ("outpath",
					       "outpath",
					       "outpath",
					       NULL,
					       G_PARAM_READWRITE |
					       G_PARAM_CONSTRUCT |
					       G_PARAM_STATIC_STRINGS |
					       G_PARAM_EXPLICIT_NOTIFY);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

RComFile *
rcom_file_new (const gchar *inpath, const gchar *outpath)
{
    return rcom_file_construct (RCOM_TYPE_FILE, inpath, outpath);
}

/**
 * rcom_file_construct:
 *
 * returns: (transfer full):
 */
RComFile *
rcom_file_construct (GType type,
		     const gchar *inpath,
		     const gchar *outpath)
{
    return g_object_new (type,
			 "inpath", inpath,
			 "outpath", outpath,
			 NULL);
}

static gboolean
inchannel_read (RComFile *self)
{
    g_autoptr (GError) error = NULL;
    GInputStream *is;
    gint8 buffer[256];
    gssize readno;

    is = rcom_file_get_input (self, &error);

    if (error) {
	goto on_error;
    }

    readno = g_input_stream_read (is, buffer, sizeof (buffer),
				  NULL, &error);

    if (error) {
	goto on_error;
    }

    if (readno > 0) {
	rcom_target_emit_feed (RCOM_TARGET (self), readno, buffer);
    }
    return TRUE;

on_error:
    emit_error (self, error);
    return FALSE;
}

static gboolean
inchannel_in_event (GIOChannel *channel, GIOCondition cond, gpointer user_data)
{
    return inchannel_read (RCOM_FILE (user_data));
}

static void
inchannel_update (RComFile *self, GFile *file, gboolean exists)
{
    RComFilePrivate *p = rcom_file_get_instance_private (self);
    g_autofree gchar *filepath = NULL;

    filepath = file ? g_file_get_path (file) : NULL;

    clear_inchannel (self);

    if (exists) {
	p->inchannel = g_io_channel_new_file (filepath, "r", NULL);

	if (p->inchannel) {
	    p->insource = g_io_add_watch (p->inchannel, G_IO_IN,
					  inchannel_in_event, self);
	}
    }
}

static void
on_inmonitor_changed (RComFile *self, GFile *file, GFile *other,
		      GFileMonitorEvent event)
{
    switch (event) {
    case G_FILE_MONITOR_EVENT_CREATED:
	inchannel_update (self, file, TRUE);
	break;

    case G_FILE_MONITOR_EVENT_DELETED:
	inchannel_update (self, file, FALSE);
	break;

    default:
	break;
    }
}

void
rcom_file_set_inpath (RComFile *self, const gchar *inpath)
{
    RComFilePrivate *p = rcom_file_get_instance_private (self);

    if (g_strcmp0 (inpath, p->inpath)) {
	g_clear_object (&p->infile);
	g_clear_object (&p->inmonitor);
	clear_inchannel (self);

	p->inpath = mk_string_assign (p->inpath, inpath);

	if (inpath) {
	    if (rcom_file_is_same_io (self)) {
		p->infile = g_object_ref (p->outfile);
		p->inmonitor = g_object_ref (p->outmonitor);
	    } else {
		p->infile = g_file_new_for_path (inpath);
		p->inmonitor = g_file_monitor (p->infile,
					       G_FILE_MONITOR_NONE,
					       NULL, NULL);
	    }

	    g_warn_if_fail (p->infile);
	    g_warn_if_fail (p->inmonitor);

	    g_signal_connect_object (p->inmonitor, "changed",
				     G_CALLBACK (on_inmonitor_changed),
				     self, G_CONNECT_SWAPPED);

	    inchannel_update (self, p->infile,
			      g_file_query_exists (p->infile, NULL));
	}

	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_INPATH]);
    }
}

const gchar *
rcom_file_get_inpath (RComFile *self)
{
    RComFilePrivate *p = rcom_file_get_instance_private (self);

    return p->inpath;
}

void
rcom_file_set_outpath (RComFile *self, const gchar *outpath)
{
    RComFilePrivate *p = rcom_file_get_instance_private (self);

    if (g_strcmp0 (outpath, p->outpath)) {
	g_clear_object (&p->outfile);
	g_clear_object (&p->outmonitor);

	p->outpath = mk_string_assign (p->outpath, outpath);

	if (outpath) {
	    if (rcom_file_is_same_io (self)) {
		p->outfile = g_object_ref (p->infile);
		p->outmonitor = g_object_ref (p->inmonitor);
	    } else {
		p->outfile = g_file_new_for_path (outpath);
		p->outmonitor = g_file_monitor (p->outfile,
						G_FILE_MONITOR_NONE,
						NULL, NULL);
	    }

	    g_warn_if_fail (p->outfile);
	    g_warn_if_fail (p->outmonitor);
	}

	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_OUTPATH]);
    }
}

const gchar *
rcom_file_get_outpath (RComFile *self)
{
    RComFilePrivate *p = rcom_file_get_instance_private (self);

    return p->outpath;
}

static GIOStream *
get_output_io (RComFile *self, GError **error)
{
    RComFilePrivate *p = rcom_file_get_instance_private (self);
    GError *local_error = NULL;
    GFileIOStream *io;

    if (!p->ostream) {
	rcom_file_mkoutput (self, p->outfile, &local_error);

	if (local_error) {
	    g_propagate_error (error, local_error);
	    return NULL;
	}

	io = g_file_open_readwrite (p->outfile, NULL, error);

	if (io) {
	    p->ostream = G_IO_STREAM (io);
	}
    }

    return p->ostream;
}

/**
 * rcom_file_get_input: (skip)
 *
 * returns: (transfer none):
 */
GInputStream *
rcom_file_get_input (RComFile *self, GError **error)
{
    RComFilePrivate *p = rcom_file_get_instance_private (self);
    GError *local_error = NULL;
    GFileInputStream *in;
    GIOStream *io;

    if (!p->istream) {
	if (rcom_file_is_same_io (self)) {
	    io = get_output_io (self, error);

	    if (!io) {
		return NULL;
	    }

	    p->istream = g_io_stream_get_input_stream (io);
	    p->istream = p->istream ? g_object_ref (p->istream) : NULL;
	} else {
	    rcom_file_mkinput (self, p->infile, &local_error);

	    if (local_error) {
		g_propagate_error (error, local_error);
		return NULL;
	    }

	    in = g_file_read (p->infile, NULL, error);

	    if (!in) {
		return NULL;
	    }

	    p->istream = G_INPUT_STREAM (in);
	}
    }

    return p->istream;
}

/**
 * rcom_file_get_output: (skip)
 *
 * returns: (transfer none):
 */
GOutputStream *
rcom_file_get_output (RComFile *self, GError **error)
{
    GIOStream *io = get_output_io (self, error);

    if (io) {
	return g_io_stream_get_output_stream (io);
    }

    return NULL;
}

void
rcom_file_connect (RComFile *self, GError **error)
{
    GError *local_error = NULL;

    rcom_file_get_output (self, &local_error);

    if (local_error) {
	g_propagate_error (error, local_error);
	return;
    }

    rcom_file_get_input (self, error);
}

gboolean
rcom_file_is_same_io (RComFile *self)
{
    return g_strcmp0 (rcom_file_get_inpath (self),
		      rcom_file_get_outpath (self)) == 0;
}

/**
 * rcom_file_mkinput: (virtual mkinput) (skip)
 */
void
rcom_file_mkinput (RComFile *self, GFile *file, GError **error)
{
    return RCOM_FILE_GET_CLASS (self)->mkinput (self, file, error);
}

/**
 * rcom_file_mkoutput: (virtual mkoutput) (skip)
 */
void
rcom_file_mkoutput (RComFile *self, GFile *file, GError **error)
{
    return RCOM_FILE_GET_CLASS (self)->mkoutput (self, file, error);
}
