#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define RCOM_TYPE_FILE (rcom_file_get_type ())

G_DECLARE_DERIVABLE_TYPE (RComFile, rcom_file,
			  RCOM, FILE, GObject);

struct _RComFileClass
{
    GObjectClass parent_class;

    void (*mkinput) (RComFile *self, GFile *file, GError **error);
    void (*mkoutput) (RComFile *self, GFile *file, GError **error);
};

RComFile       *rcom_file_new		(const gchar *inpath,
					 const gchar *outpath);
RComFile       *rcom_file_construct	(GType type,
					 const gchar *inpath,
					 const gchar *outpath);
void		rcom_file_set_inpath    (RComFile *self,
					 const gchar *inpath);
const gchar    *rcom_file_get_inpath	(RComFile *self);
void		rcom_file_set_outpath   (RComFile *self,
					 const gchar *outpath);
const gchar    *rcom_file_get_outpath	(RComFile *self);
GInputStream   *rcom_file_get_input	(RComFile *self,
					 GError **error);
GOutputStream  *rcom_file_get_output	(RComFile *self,
					 GError **error);
void		rcom_file_connect	(RComFile *self,
					 GError **error);
gboolean	rcom_file_is_same_io	(RComFile *self);
void		rcom_file_mkinput	(RComFile *self,
					 GFile *file,
					 GError **error);
void		rcom_file_mkoutput	(RComFile *self,
					 GFile *file,
					 GError **error);

G_END_DECLS
