#include <bcom-glib.h>
#include <stdio.h>

static void
receive_ping (BComPort *port, guint ack, BComPort *response)
{
    g_message ("ping %u", ack);

    if (ack > 0) {
	g_message ("pong %u", ack - 1);
	bcom_port_send_ping (response, ack - 1, NULL);
    }
}

int main ()
{
    /*
     * basically this example is the same as bcom-ping one but
     * using the glib wrapper in C
     */
    g_autoptr (BComPort) port0 = NULL;
    g_autoptr (BComPort) port1 = NULL;
    g_autoptr (RComSerial) serial0 = NULL;
    g_autoptr (RComSerial) serial1 = NULL;

    port0 = bcom_port_new ();
    port1 = bcom_port_new ();
    serial0 = rcom_serial_new ();
    serial1 = rcom_serial_new ();

    rcom_serial_bind_port (serial0, RCOM_PORT (port0));
    rcom_serial_bind_port (serial1, RCOM_PORT (port1));

    g_signal_connect_object (port0, "write-data",
			     G_CALLBACK (rcom_port_feed_data), port1,
			     G_CONNECT_SWAPPED);
    g_signal_connect_object (port1, "write-data",
			     G_CALLBACK (rcom_port_feed_data), port0,
			     G_CONNECT_SWAPPED);

    g_signal_connect (port0, "receive-ping",
		      G_CALLBACK (receive_ping), port1);
    g_signal_connect (port1, "receive-ping",
		      G_CALLBACK (receive_ping), port0);

    bcom_port_send_ping (port0, 100, NULL);

    return 0;
}
