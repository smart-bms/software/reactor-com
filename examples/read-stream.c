#include <bcom.h>
#include <stdio.h>

static void
receive_message (struct bcom_port *port, char *message)
{
    printf ("parsed message: %s\n", message);
}

static struct bcom_receive receive = {
    .message = receive_message,
};

int main ()
{
    struct rcom_serial serial0;
    struct bcom_port port0;

    bcom_port_init (&port0, &receive, NULL, NULL);
    rcom_serial_init (&serial0, &port0.base);

    uint8_t data[] = {
	0xff,
	0x00,
	0x02,
	0x0d,
	0xaa,
	0x2d,
	0x68,
	0x65,
	0x6c,
	0x6c,
	0x6f,
	0x2c,
	0x20,
	0x72,
	0x63,
	0x6f,
	0x6d,
	0x21,
	0x00,
	0xde,
	0x55,
    };

    bcom_port_feed (&port0, sizeof (data), data);

    return 0;
}
