#include "rcom-port.h"
#include "rcom-target.h"

#define rcom_port_init(...) __rcom_port_init (__VA_ARGS__)
#include "rcom-core.h"
#undef rcom_port_init

typedef struct {
    struct rcom_port *port;
    RComTarget *target;

    GDestroyNotify parse_destroy;
    GDestroyNotify serialize_destroy;
    GDestroyNotify deserialize_destroy;
} RComPortPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (RComPort, rcom_port,
				     G_TYPE_OBJECT);

enum
{
    PROP_0,
    PROP_PORT_SIZE,
    PROP_TARGET,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];
static guint sig_write_data;

static void
finalize (GObject *gobj)
{
    RComPort *self = RCOM_PORT (gobj);
    RComPortPrivate *p = rcom_port_get_instance_private (self);

    rcom_port_set_target (self, NULL);
    rcom_port_set_serialize (self, NULL, NULL, NULL);
    rcom_port_set_deserialize (self, NULL, NULL, NULL);

    g_free (p->port);

    G_OBJECT_CLASS (rcom_port_parent_class)->finalize (gobj);
}

static void
set_property (GObject *gobj, guint id,
	      const GValue *value, GParamSpec *spec)
{
    RComPort *self = RCOM_PORT (gobj);
    RComPortPrivate *p = rcom_port_get_instance_private (self);

    switch (id) {
    case PROP_PORT_SIZE:
	p->port = g_malloc0 (g_value_get_uint (value));
	break;

    case PROP_TARGET:
	rcom_port_set_target (self, g_value_get_object (value));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
get_property (GObject *gobj, guint id,
	      GValue *value, GParamSpec *spec)
{
    RComPort *self = RCOM_PORT (gobj);

    switch (id) {
    case PROP_TARGET:
	g_value_set_object (value, rcom_port_get_target (self));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static gint
on_write_data (RComPort *self, guint size, gconstpointer data)
{
    RComTarget *target;
    target = rcom_port_get_target (self);

    if (target) {
	return rcom_target_write_data (target, size, data);
    }

    return 0;
}

static void
rcom_port_init (RComPort *self)
{
}

static gboolean
accumulate_write_result (GSignalInvocationHint *ihint,
			 GValue *return_value,
			 const GValue *handler_return,
			 gpointer user_data)
{
    g_value_set_int (return_value,
		     MAX (g_value_get_int (return_value),
			  g_value_get_int (handler_return)));

    return TRUE;
}

static void
rcom_port_class_init (RComPortClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    cls->on_write_data = on_write_data;
    gcls->finalize = finalize;
    gcls->set_property = set_property;
    gcls->get_property = get_property;

    /**
     * RComPort::write-data
     * @self:
     * @size:
     * @data: (array length=size) (element-type guint8):
     */
    sig_write_data = g_signal_new ("write-data",
				   RCOM_TYPE_PORT,
				   G_SIGNAL_RUN_FIRST,
				   G_STRUCT_OFFSET (RComPortClass,
						    on_write_data),
				   accumulate_write_result,
				   NULL, NULL,
				   G_TYPE_INT, 2,
				   G_TYPE_UINT,
				   G_TYPE_POINTER);

    props[PROP_PORT_SIZE] = g_param_spec_uint ("port-size",
					       "port-size",
					       "port-size",
					       0, G_MAXUINT32, 0,
					       G_PARAM_WRITABLE |
					       G_PARAM_CONSTRUCT_ONLY |
					       G_PARAM_STATIC_STRINGS);

    props[PROP_TARGET] = g_param_spec_object ("target",
					      "target",
					      "target",
					      RCOM_TYPE_TARGET,
					      G_PARAM_READWRITE |
					      G_PARAM_CONSTRUCT |
					      G_PARAM_STATIC_STRINGS |
					      G_PARAM_EXPLICIT_NOTIFY);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

/**
 * rcom_port_construct:
 * 
 * returns: (transfer full):
 */
RComPort *
rcom_port_construct (GType type, guint port_size)
{
    g_assert (g_type_is_a (type, RCOM_TYPE_PORT));

    return g_object_new (type, "port-size", port_size, NULL);
}

void
rcom_port_set_target (RComPort *self, RComTarget *target)
{
    RComPortPrivate *p = rcom_port_get_instance_private (self);

    if (target != p->target) {
	if (p->target) {
	    g_signal_handlers_disconnect_by_func (p->target,
						  rcom_port_feed,
						  self);
	}

	g_set_object (&p->target, target);

	if (p->target) {
	    g_signal_connect_object (p->target, "feed",
				     G_CALLBACK (rcom_port_feed_data), self,
				     G_CONNECT_SWAPPED);
	}

	g_object_notify_by_pspec (G_OBJECT (self), props[PROP_TARGET]);
    }
}

/**
 * rcom_port_get_target:
 *
 * returns: (transfer none):
 */
RComTarget *
rcom_port_get_target (RComPort *self)
{
    RComPortPrivate *p = rcom_port_get_instance_private (self);

    return p->target;
}

gint
rcom_port_feed_data (RComPort *self, guint size, gconstpointer data)
{
    RComPortPrivate *p = rcom_port_get_instance_private (self);

    rcom_port_feed (p->port, size, data);

    return size;
}

/**
 * rcom_port_write_data:
 * @data: (array length=size) (element-type guint8):
 */
gint
rcom_port_write_data (RComPort *self, guint size, gconstpointer data)
{
    gint ret;

    g_signal_emit (self, sig_write_data, 0, size, data, &ret);

    return ret;
}

gpointer
rcom_port_get_port (RComPort *self)
{
    RComPortPrivate *p = rcom_port_get_instance_private (self);

    return p->port;
}

void
rcom_port_set_parse (RComPort *self,
		     RComParse parse,
		     gpointer parse_closure,
		     GDestroyNotify parse_destroy)
{
    RComPortPrivate *p = rcom_port_get_instance_private (self);

    if (p->port && p->port->parse_closure && p->parse_destroy) {
	p->parse_destroy (p->port->parse_closure);
    }

    p->port->parse = parse;
    p->port->parse_closure = parse_closure;
    p->parse_destroy = parse_destroy;
}

void
rcom_port_set_serialize (RComPort *self,
			 RComSerialize serialize,
			 gpointer serialize_closure,
			 GDestroyNotify serialize_destroy)
{
    RComPortPrivate *p = rcom_port_get_instance_private (self);

    if (p->port && p->port->serialize_closure && p->serialize_destroy) {
	p->serialize_destroy (p->port->serialize_closure);
    }

    p->port->serialize = serialize;
    p->port->serialize_closure = serialize_closure;
    p->serialize_destroy = serialize_destroy;
}

void
rcom_port_set_deserialize (RComPort *self,
			   RComDeserialize deserialize,
			   gpointer deserialize_closure,
			   GDestroyNotify deserialize_destroy)
{
    RComPortPrivate *p = rcom_port_get_instance_private (self);

    if (p->port && p->port->deserialize_closure && p->deserialize_destroy) {
	p->deserialize_destroy (p->port->deserialize_closure);
    }

    p->port->deserialize = deserialize;
    p->port->deserialize_closure = deserialize_closure;
    p->deserialize_destroy = deserialize_destroy;
}
