#include "main.h"

#define emit(...) *code = mk_string_append (*code, __VA_ARGS__)

static void
h_enum (MkString **code, Enumeration *e)
{
    MK_ARRAY_FOREACH (e->value_v, Value *, v) {
	emit ("#define %s_%s_%s %u\n",
	      e->p->prefix_upper,
	      e->name_upper,
	      v->name_upper,
	      v->value);
    }
}

static void
h_struct (MkString **code, Structure *s)
{
    if (s->ext) {
	return;
    }

    emit ("%s\n"
	  "{\n"
	  "<indent>",
	  s->type->stdc);

    MK_ARRAY_FOREACH (s->field_v, Field *, f) {
	emit ("%s %s;\n",
	      f->type->stdc, f->name_lower);
    }

    emit ("</indent>"
	  "};\n");
}

static void
generate_h (MkString **code, Protocol *p, Options *o)
{
    emit ("#pragma once\n"
	  "#include \"rcom-core.h\"\n");

    MK_ARRAY_FOREACH (p->include_v, MkString *, s) {
	emit ("#include \"%s\"\n", s);
    }

    MK_ARRAY_FOREACH (p->enumeration_v, Enumeration *, e) {
	h_enum (code, e);
    }

    MK_ARRAY_FOREACH (p->structure_v, Structure *, s) {
	h_struct (code, s);
    }

    emit ("struct %s_port\n"
	  "{\n"
	  "<indent>"
	  "struct rcom_port base;\n"
	  "struct %s_receive *receive;\n"
	  "</indent>"
	  "};\n",
	  p->prefix_lower, p->prefix_lower);

    emit ("struct %s_receive\n"
	  "{\n"
	  "<indent>",
	  p->prefix_lower);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("void (*%s) (<align>%s *__port",
	      m->name_lower, p->port_type->stdc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n%s %s%s", a->type->stdc,
		  a->length || a->type->s ? "*" : "", a->name);
	}

	emit ("</align>);\n");
    }

    emit ("</indent>"
	  "};\n");

    emit ("void %s_port_bind (%s *port);\n",
	  p->prefix_lower, p->port_type->stdc);

    emit ("void %s_port_feed (<align>%s *port,\n"
	  "size_t size,\n"
	  "const void *data</align>);\n",
	  p->prefix_lower, p->port_type->stdc);

    emit ("void %s_port_init (<align>%s *port,\n"
	  "struct %s_receive *receive,\n"
	  "rcom_write_t write,\n"
	  "void *write_closure</align>);\n",
	  p->prefix_lower, p->port_type->stdc, p->prefix_lower);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("int %s_port_%s (<align>%s *__port",
	      p->prefix_lower,
	      m->name_lower, p->port_type->stdc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n%s ", a->type->stdc);

	    if (a->type->s || a->length) {
		emit ("*");
	    }

	    emit ("%s", a->name);
	}

	emit ("</align>);\n");
    }

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("int %s_%s (<align>",
	      p->prefix_lower,
	      m->name_lower);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    if (MK_ARRAY_FOREACH_INDEX (a) > 0) {
		emit (",\n");
	    }

	    emit ("%s ", a->type->stdc);

	    if (a->type->s || a->length) {
		emit ("*");
	    }

	    emit ("%s", a->name);
	}

	emit ("</align>);\n");
    }
}

void
generate_c (MkString **code, Protocol *p, Options *o)
{
    emit ("#include \"%s\"\n"
	  "#include <string.h>\n"
	  "#include <stdio.h>\n",
	  mk_string_path_basename (mk_string_newa (o->header)));

    emit ("enum\n"
	  "{\n"
	  "<indent>"
	  "%s_MESSAGE_0,\n",
	  p->prefix_upper);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("%s_MESSAGE_%s,\n",
	      p->prefix_upper, m->name_upper);
    }

    emit ("%s_NUM_MESSAGES,\n"
	  "</indent>"
	  "};\n",
	  p->prefix_upper);

    emit ("static %s *%s_bound;\n",
	  p->port_type->stdc, p->prefix_lower);

    MK_ARRAY_FOREACH (p->message_v, Message *, m)  {
	emit ("static int\n"
	      "%s_parse_%s (<align>size_t __size, const void *__data,\n"
	      "void *__closure</align>)\n"
	      "{\n"
	      "<indent>"
	      "%s *__port;\n"
	      "size_t __offset, __length;\n",
	      p->prefix_lower, m->name_lower,
	      p->port_type->stdc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit ("%s %s%s;\n", a->type->stdc,
		  a->length || a->type->s ? "*" : "", a->name_lower);
	}

	emit ("__port = __closure;\n"
	      "__offset = 0;\n");

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    if (a->length || a->type->s) {
		emit ("%s = (void *) ((uint8_t *) __data + __offset);\n",
		      a->name_lower);

		if (g_strcmp0 (a->length, "@strlen") == 0) {
		    emit ("__length = strlen (%s) + 1;\n",
			  a->name_lower);
		} else {
		    emit ("__length = %s;\n",
			  a->length ? a->length : "1");
		}
	    } else {
		emit ("memcpy (&%s, (uint8_t *) __data + __offset, sizeof (%s));\n"
		      "__length = 1;\n",
		      a->name, a->name);
	    }

	    emit ("__offset += sizeof (%s) * __length;\n",
		  a->type->stdc);
	}

	emit ("if (__offset != __size) {\n"
	      "<indent>"
	      "fprintf (stderr, \"__offset != __size\\n\");\n"
	      "return -1;\n"
	      "</indent>"
	      "}\n");

	emit ("if (!__port->receive || !__port->receive->%s) {\n"
	      "<indent>"
	      "return -1;\n"
	      "</indent>"
	      "}\n"
	      "__port->receive->%s (__port",
	      m->name_lower, m->name_lower);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (", %s", a->name_lower);
	}

	emit (");\n"
	      "return 0;\n"
	      "</indent>"
	      "}\n");
    }

    emit ("static int\n"
	  "%s_parse (<align>uint8_t code, size_t size,\n"
	  "const void *data, void *closure</align>)\n"
	  "{\n"
	  "<indent>"
	  "switch (code) {\n",
	  p->prefix_lower);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("case %s_MESSAGE_%s:\n"
	      "<indent>"
	      "return %s_parse_%s (size, data, closure);\n"
	      "</indent>",
	      p->prefix_upper, m->name_upper,
	      p->prefix_lower, m->name_lower);
    }

    emit ("default:\n"
	  "<indent>"
	  "return -1;\n"
	  "</indent>"
	  "}\n"
	  "</indent>"
	  "}\n");

    emit ("void\n"
	  "%s_port_bind (%s *port)\n"
	  "{\n"
	  "<indent>"
	  "%s_bound = port;\n"
	  "</indent>"
	  "}\n",
	  p->prefix_lower, p->port_type->stdc, p->prefix_lower);

    emit ("void %s_port_feed (<align>%s *port,\n"
	  "size_t size,\n"
	  "const void *data</align>)\n"
	  "{\n"
	  "<indent>"
	  "rcom_port_feed (&port->base, size, data);\n"
	  "</indent>"
	  "}\n",
	  p->prefix_lower, p->port_type->stdc);

    emit ("void\n"
	  "%s_port_init (<align>%s *port,\n"
	  "struct %s_receive *receive,\n"
	  "rcom_write_t write,\n"
	  "void *write_closure</align>)\n"
	  "{\n"
	  "<indent>"
	  "memset (port, 0, sizeof (*port));\n"
	  "port->base.write = write;\n"
	  "port->base.write_closure = write_closure;\n"
	  "port->base.parse = %s_parse;\n"
	  "port->base.parse_closure = port;\n"
	  "port->receive = receive;\n"
	  "</indent>"
	  "}\n",
	  p->prefix_lower, p->port_type->stdc, p->prefix_lower,
	  p->prefix_lower);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("int\n"
	      "%s_port_%s (<align>%s *__port",
	      p->prefix_lower, m->name_lower, p->port_type->stdc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n"
		  "%s %s%s",
		  a->type->stdc, a->length || a->type->s ?
		  "*" : "", a->name_lower);
	}

	emit ("</align>)\n"
	      "{\n"
	      "<indent>"
	      "const void *__data_v[%u];\n"
	      "size_t __size_v[%u];\n",
	      mk_array_len (m->arg_v),
	      mk_array_len (m->arg_v));

	emit ("if (!__port) {\n"
	      "<indent>"
	      "__port = %s_bound;\n"
	      "if (!__port) {\n"
	      "<indent>"
	      "return -1;\n"
	      "</indent>"
	      "}\n"
	      "</indent>"
	      "}\n",
	      p->prefix_lower);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    if (a->length || a->type->s) {
		emit ("__data_v[%u] = %s;\n"
		      "__size_v[%u] = sizeof (*%s)",
		      MK_ARRAY_FOREACH_INDEX (a), a->name_lower,
		      MK_ARRAY_FOREACH_INDEX (a), a->name_lower);

		if (g_strcmp0 (a->length, "@strlen") == 0) {
		    emit (" * (strlen (%s) + 1)", a->name_lower);
		} else if (a->length) {
		    emit (" * %s", a->length);
		}

		emit (";\n");
	    } else {
		emit ("__data_v[%u] = &%s;\n"
		      "__size_v[%u] = sizeof (%s);\n",
		      MK_ARRAY_FOREACH_INDEX (a), a->name_lower,
		      MK_ARRAY_FOREACH_INDEX (a), a->name_lower);
	    }
	}

	emit ("return rcom_port_send (<align>&__port->base, %s_MESSAGE_%s,\n"
	      "%u, __size_v, __data_v</align>);\n",
	      p->prefix_upper, m->name_upper, mk_array_len (m->arg_v));

	emit ("</indent>"
	      "}\n");
    }

    emit ("void __%s_port_init () __attribute__ ((alias (\"%s_port_init\")));\n",
	  p->prefix_lower, p->prefix_lower);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("int\n"
	      "%s_%s (<align>",
	      p->prefix_lower,
	      m->name_lower);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    if (MK_ARRAY_FOREACH_INDEX (a) > 0) {
		emit (",\n");
	    }

	    emit ("%s ", a->type->stdc);

	    if (a->type->s || a->length) {
		emit ("*");
	    }

	    emit ("%s", a->name);
	}

	emit ("</align>)\n"
	      "{\n"
	      "<indent>"
	      "return %s_port_%s (0",
	      p->prefix_lower, m->name_lower);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (", %s", a->name);
	}

	emit (");\n"
	      "</indent>"
	      "}\n");
    }
}

void
generate_core (MkString **h_code, MkString **c_code,
	       Protocol *p, Options *o)
{
    generate_h (h_code, p, o);
    generate_c (c_code, p, o);
}
