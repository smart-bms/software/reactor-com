#include "main.h"

#define emit(...) *code = mk_string_append (*code, __VA_ARGS__)

static void
generate_h (MkString **code, Protocol *p, Options *o)
{
    g_autoptr (MkString) tmp = NULL;
    Enumeration *e;
    Structure *s;
    Field *f;
    Value *v;
    guint i, k;

    emit ("#pragma once\n"
	  "#include <rcom-glib.h>\n"
	  "#include \"%s\"\n",
	  mk_string_path_basename (mk_string_newa (o->header)));

    for (i = 0; i < mk_array_len (p->glib_include_v); i++) {
	emit ("#include \"%s\"\n",
	      mk_array_get (p->glib_include_v, i));
    }

    for (i = 0; i < mk_array_len (p->enumeration_v); i++) {
	e = mk_array_get (p->enumeration_v, i);

	emit ("#define %s_TYPE_%s (%s_%s_get_type ())\n"
	      "typedef enum {\n"
	      "<indent>",
	      p->prefix_upper, e->name_upper,
	      p->prefix_lower, e->name_lower);

	for (k = 0; k < mk_array_len (e->value_v); k++) {
	    v = mk_array_get (e->value_v, k);
	    tmp = mk_string_assign (tmp, p->prefix_upper);
	    tmp = mk_string_append (tmp, "_%s_%s",
				    e->name_upper, v->name_upper);

	    emit ("#undef %s\n"
		  "%s = %u,\n",
		  tmp, tmp, v->value);
	}

	emit ("</indent>"
	      "} %s%s;\n"
	      "GType\n"
	      "%s_%s_get_type () G_GNUC_CONST;\n",
	      p->prefix_camel, e->name_camel,
	      p->prefix_lower, e->name_lower);
    }

    for (i = 0; i < mk_array_len (p->structure_v); i++) {
	s = mk_array_get (p->structure_v, i);

	if (s->ext) {
	    continue;
	}

	emit ("#define %s (%s_get_type ())\n",
	      s->type->gtype,
	      s->prefix_lower);

	emit ("G_DECLARE_FINAL_TYPE (<align>%s, %s,\n"
	      "%s, %s, RComStructure</align>);\n",
	      s->type->glibc,
	      s->prefix_lower,
	      p->prefix_upper, s->name_upper);

	emit ("%s *%s_new (void);\n"
	      "%s *%s_new_wrap (%s *raw, gboolean copy);\n"
	      "const %s *%s_raw (%s *self);\n",
	      s->type->glibc, s->prefix_lower,
	      s->type->glibc, s->prefix_lower, s->type->stdc,
	      s->type->stdc, s->prefix_lower, s->type->glibc);

	for (k = 0; k < mk_array_len (s->field_v); k++) {
	    f = mk_array_get (s->field_v, k);

	    emit ("void %s_set_%s (<align>%s *self,\n"
		  "%s %s%s</align>);\n"
		  "%s %s%s_get_%s (%s *self);\n",
		  s->prefix_lower, f->name_lower, s->type->glibc,
		  f->type->glibc,
		  f->type->s ? "*" : "",
		  f->name_lower,
		  f->type->glibc,
		  f->type->s ? "*" : "",
		  s->prefix_lower, f->name_lower,
		  s->type->glibc);
	}
    }

    emit ("#define %s (%s_port_get_type ())\n"
	  "G_DECLARE_DERIVABLE_TYPE (<align>%s, %s_port,\n"
	  "%s, PORT, RComPort</align>);\n",
	  p->port_type->gtype, p->prefix_lower,
	  p->port_type->glibc, p->prefix_lower,
	  p->prefix_upper);

    emit ("struct _%sClass\n"
	  "{\n"
	  "<indent>"
	  "RComPortClass parent_class;\n"
	  "gint (*on_write_data) (<align>%s *self,\n"
	  "gsize size,\n"
	  "gconstpointer data</align>);\n",
	  p->port_type->glibc,
	  p->port_type->glibc);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("void (*on_receive_%s) (<align>%s *port",
	      m->name_lower, p->port_type->glibc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n%s %s%s",
		  a->type->glibc,
		  a->length || a->type->s ? "*" : "",
		  a->name_lower);
	}

	emit ("</align>);\n");
    }

    emit ("</indent>"
	  "};\n");

    emit ("%s *%s_new (void);\n",
	  p->port_type->glibc, p->port_type->s->prefix_lower);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("gint %s_send_%s (<align>%s *self",
	      p->port_struct->prefix_lower,
	      m->name_lower,
	      p->port_type->glibc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n%s %s%s",
		  a->type->glibc,
		  a->length || a->type->s ? "*" : "",
		  a->name_lower);
	}

	emit (",\nGError **error</align>);\n");
    }

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("void %s_emit_receive_%s (<align>%s *self",
	      p->port_struct->prefix_lower,
	      m->name_lower,
	      p->port_type->glibc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n%s %s%s",
		  a->type->glibc,
		  a->length || a->type->s ? "*" : "",
		  a->name_lower);
	}

	emit ("</align>);\n");
    }
}

static void
generate_c (MkString **code, Protocol *p, Options *o)
{
    Enumeration *e;
    Value *v;
    Structure *s;
    Field *f;
    guint i, k;

    emit ("#define %s_init(...) __%s_init (__VA_ARGS__)\n"
	  "#include \"%s\"\n"
	  "#undef %s_init\n",
	  p->port_struct->prefix_lower,
	  p->port_struct->prefix_lower,
	  mk_string_path_basename (mk_string_newa (o->glib_header)),
	  p->port_struct->prefix_lower);

    for (i = 0; i < mk_array_len (p->enumeration_v); i++) {
	e = mk_array_get (p->enumeration_v, i);

	emit ("GType\n"
	      "%s_%s_get_type ()\n"
	      "{\n"
	      "<indent>"
	      "static const GEnumValue values[] = {\n"
	      "<indent>",
	      p->prefix_lower, e->name_lower);

	for (k = 0; k < mk_array_len (e->value_v); k++) {
	    v = mk_array_get (e->value_v, k);

	    emit ("{ <align>%s_%s_%s,\n\"%s_%s_%s\",\n\"%s\"</align> },\n",
		  p->prefix_upper, e->name_upper, v->name_upper,
		  p->prefix_upper, e->name_upper, v->name_upper,
		  v->name);
	}

	emit ("{ 0, NULL, NULL },\n"
	      "</indent>"
	      "};\n"
	      "static GType type;\n"
	      "GType t;\n"
	      "if (g_once_init_enter (&type)) {\n"
	      "<indent>"
	      "t = g_enum_register_static (\"%s%s\", values);\n"
	      "g_once_init_leave (&type, t);\n"
	      "</indent>"
	      "}\n"
	      "return type;\n"
	      "</indent>"
	      "}\n",
	      p->prefix_camel, e->name_camel);
    }

    for (i = 0; i < mk_array_len (p->structure_v); i++) {
	s = mk_array_get (p->structure_v, i);

	if (s->ext) {
	    continue;
	}

	emit ("struct _%s\n"
	      "{\n"
	      "<indent>"
	      "RComStructure parent_instance;\n"
	      "%s *raw;\n",
	      s->type->glibc, s->type->stdc);

	for (k = 0; k < mk_array_len (s->field_v); k++) {
	    f = mk_array_get (s->field_v, k);

	    if (f->type->s) {
		emit ("%s %s%s;\n",
		      f->type->glibc, f->type->s ? "*" : "",
		      f->name_lower);
	    }
	}

	emit ("</indent>"
	      "};\n");

	emit ("G_DEFINE_TYPE (<align>%s, %s,\n"
	      "RCOM_TYPE_STRUCTURE</align>);\n"
	      "enum\n"
	      "{\n"
	      "<indent>"
	      "%s_PROP_0,\n",
	      s->type->glibc, s->prefix_lower,
	      s->prefix_upper);

	for (k = 0; k < mk_array_len (s->field_v); k++) {
	    f = mk_array_get (s->field_v, k);

	    emit ("%s_PROP_%s,\n",
		  s->prefix_upper, f->name_upper);
	}

	emit ("%s_N_PROPS,\n"
	      "</indent>"
	      "};\n"
	      "static GParamSpec *%s_props[%s_N_PROPS];\n",
	      s->prefix_upper, s->prefix_lower, s->prefix_upper);

	emit ("static void\n"
	      "%s_init (%s *self)\n"
	      "{\n"
	      "}\n",
	      s->prefix_lower, s->type->glibc);

	emit ("static void\n"
	      "%s_constructed (GObject *gobj)\n"
	      "{\n"
	      "<indent>"
	      "G_OBJECT_CLASS (%s_parent_class)->constructed (gobj);\n"
	      "%s *self = %s (gobj);\n"
	      "self->raw = rcom_structure_get_raw (RCOM_STRUCTURE (gobj));\n",
	      s->prefix_lower,
	      s->prefix_lower,
	      s->type->glibc,
	      s->prefix_upper);

	for (k = 0; k < mk_array_len (s->field_v); k++) {
	    f = mk_array_get (s->field_v, k);

	    if (f->type->s) {
		emit ("self->%s = %s_new_wrap (<align>&self->raw->%s,\n"
		      "FALSE</align>);\n",
		      f->name_lower, f->type->s->prefix_lower,
		      f->name_lower);
	    }
	}

	emit ("</indent>"
	      "}\n");

	emit ("static void\n"
	      "%s_finalize (GObject *gobj)\n"
	      "{\n"
	      "<indent>"
	      "%s *self = %s (gobj);\n"
	      "(void) self;\n",
	      s->prefix_lower,
	      s->type->glibc,
	      s->prefix_upper);

	for (k = 0; k < mk_array_len (s->field_v); k++) {
	    f = mk_array_get (s->field_v, k);

	    if (f->type->s) {
		emit ("g_object_unref (self->%s);\n",
		      f->name_lower);
	    }
	}

	emit ("G_OBJECT_CLASS (%s_parent_class)->finalize (gobj);\n"
	      "</indent>"
	      "}\n",
	      s->prefix_lower);

	emit ("static void\n"
	      "%s_set_property (<align>GObject *gobj, guint id,\n"
	      "const GValue *value, GParamSpec *spec</align>)\n"
	      "{\n"
	      "<indent>"
	      "%s *self = %s (gobj);\n"
	      "switch (id) {\n",
	      s->prefix_lower, s->type->glibc, s->prefix_upper,
	      s->prefix_upper);

	for (k = 0; k < mk_array_len (s->field_v); k++) {
	    f = mk_array_get (s->field_v, k);

	    emit ("case %s_PROP_%s:\n"
		  "<indent>"
		  "%s_set_%s (self, g_value_get_%s (value));\n"
		  "break;\n"
		  "</indent>",
		  s->prefix_upper, f->name_upper,
		  s->prefix_lower, f->name_lower, f->type->gvalue);
	}

	emit ("default:\n"
	      "<indent>"
	      "G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);\n"
	      "</indent>"
	      "}\n"
	      "</indent>"
	      "}\n");

	emit ("static void\n"
	      "%s_get_property (<align>GObject *gobj, guint id,\n"
	      "GValue *value, GParamSpec *spec</align>)\n"
	      "{\n"
	      "<indent>"
	      "%s *self = %s (gobj);\n"
	      "(void) self;\n"
	      "switch (id) {\n",
	      s->prefix_lower, s->type->glibc, s->prefix_upper);

	for (k = 0; k < mk_array_len (s->field_v); k++) {
	    f = mk_array_get (s->field_v, k);

	    emit ("case %s_PROP_%s:\n"
		  "<indent>"
		  "g_value_set_%s (value, %s_get_%s (self));\n"
		  "break;\n"
		  "</indent>",
		  s->prefix_upper, f->name_upper,
		  f->type->gvalue, s->prefix_lower, f->name_lower);
	}

	emit ("default:\n"
	      "<indent>"
	      "G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);\n"
	      "</indent>"
	      "}\n"
	      "</indent>"
	      "}\n");

	emit ("static void\n"
	      "%s_class_init (%sClass *cls)\n"
	      "{\n"
	      "<indent>"
	      "GObjectClass *gcls = G_OBJECT_CLASS (cls);\n"
	      "gcls->constructed = %s_constructed;\n"
	      "gcls->finalize = %s_finalize;\n"
	      "gcls->set_property = %s_set_property;\n"
	      "gcls->get_property = %s_get_property;\n",
	      s->prefix_lower, s->type->glibc,
	      s->prefix_lower, s->prefix_lower,
	      s->prefix_lower, s->prefix_lower);

	for (k = 0; k < mk_array_len (s->field_v); k++) {
	    f = mk_array_get (s->field_v, k);

	    emit ("%s_props[%s_PROP_%s] =\n"
		  "<indent>"
		  "g_param_spec_%s (<align>\"%s\",\n"
		  "\"%s\",\n"
		  "\"%s\",\n",
		  s->prefix_lower, s->prefix_upper, f->name_upper,
		  f->type->gvalue,
		  f->name, f->name, f->name);

	    if (f->type->s) {
		emit ("%s,\n", f->type->gtype);
	    } else {
		emit ("%s, %s, 0,\n",
		      f->type->min, f->type->max);
	    }

	    emit ("G_PARAM_READWRITE |\n"
		  "G_PARAM_STATIC_STRINGS |\n"
		  "G_PARAM_EXPLICIT_NOTIFY</align>);\n"
		  "</indent>");
	}

	emit ("g_object_class_install_properties (<align>gcls,\n"
	      "%s_N_PROPS,\n"
	      "%s_props</align>);\n",
	      s->prefix_upper, s->prefix_lower);

	emit ("</indent>"
	      "}\n");

	emit ("%s *\n"
	      "%s_new ()\n"
	      "{\n"
	      "<indent>"
	      "return %s_new_wrap (NULL, FALSE);\n"
	      "</indent>"
	      "}\n"
	      "%s *\n"
	      "%s_new_wrap (%s *raw, gboolean copy)\n"
	      "{\n"
	      "<indent>"
	      "return rcom_structure_construct (<align>%s, raw,\n"
	      "copy, sizeof (%s)</align>);\n"
	      "</indent>"
	      "}\n",
	      s->type->glibc, s->prefix_lower, s->prefix_lower,
	      s->type->glibc, s->prefix_lower, s->type->stdc,
	      s->type->gtype, s->type->stdc);

	for (k = 0; k < mk_array_len (s->field_v); k++) {
	    f = mk_array_get (s->field_v, k);

	    emit ("void\n"
		  "%s_set_%s (<align>%s *self,\n%s %s%s</align>)\n"
		  "{\n"
		  "<indent>",
		  s->prefix_lower, f->name_lower, s->type->glibc,
		  f->type->glibc, f->type->s ? "*" : "", f->name_lower);

	    if (f->type->s) {
		emit ("if (rcom_structure_equal (<align>RCOM_STRUCTURE (self->%s),\n"
		      "RCOM_STRUCTURE (%s)</align>)) {\n"
		      "<indent>"
		      "return;\n"
		      "</indent>"
		      "}\n"
		      "rcom_structure_copy (<align>RCOM_STRUCTURE (%s),\n"
		      "RCOM_STRUCTURE (self->%s)</align>);\n",
		      f->name_lower, f->name_lower,
		      f->name_lower, f->name_lower);
	    } else {
		emit ("if (self->raw->%s == %s) {\n"
		      "<indent>"
		      "return;\n"
		      "</indent>"
		      "}\n"
		      "self->raw->%s = %s;\n",
		      f->name_lower, f->name_lower,
		      f->name_lower, f->name_lower);
	    }

	    emit ("g_object_notify_by_pspec (G_OBJECT (self),\n<indent>"
		  "%s_props[%s_PROP_%s]);\n</indent>",
		  s->prefix_lower, s->prefix_upper, f->name_upper);

	    emit ("</indent>"
		  "}\n");

	    if (f->type->s) {
		emit ("/**\n"
		      " * %s_get_%s:\n"
		      " * returns: (transfer none):\n"
		      " */\n",
		      s->prefix_lower, f->name_lower);
	    }

	    emit ("%s%s\n"
		  "%s_get_%s (%s *self)\n"
		  "{\n"
		  "<indent>"
		  "return self->%s%s;\n"
		  "</indent>"
		  "}\n",
		  f->type->glibc, f->type->s ? " *" : "",
		  s->prefix_lower, f->name_lower, s->type->glibc,
		  f->type->s ? "" : "raw->", f->name_lower);
	}
    }

    emit ("G_DEFINE_TYPE (%s, %s, RCOM_TYPE_PORT);\n",
	  p->port_type->glibc, p->port_struct->prefix_lower);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("static void\n"
	      "%s_on_receive_%s_default (<align>%s *port",
	      p->port_struct->prefix_lower, m->name_lower,
	      p->port_type->glibc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n%s %s%s",
		  a->type->glibc,
		  a->length || a->type->s ? "*" : "",
		  a->name_lower);
	}

	emit ("</align>)\n"
	      "{}\n");
    }

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("static guint %s_sig_receive_%s;\n",
	      p->port_struct->prefix_lower,
	      m->name_lower);
    }

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("static void\n"
	      "%s_forward_receive_%s (<align>%s *port",
	      p->port_struct->prefix_lower,
	      m->name_lower,
	      p->port_type->stdc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n%s %s%s",
		  a->type->stdc,
		  a->length || a->type->s ? "*" : "",
		  a->name_lower);
	}

	emit ("</align>)\n"
	      "{\n"
	      "<indent>");

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    if (a->type->s) {
		emit ("g_autoptr (%s) __%s = %s_new_wrap (%s, TRUE);\n",
		      a->type->glibc, a->name_lower,
		      a->type->s->prefix_lower, a->name_lower);
	    }
	}

	emit ("%s_emit_receive_%s (<align>%s (port->base.context)",
	      p->port_struct->prefix_lower,
	      m->name_lower,
	      p->port_struct->prefix_upper);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n%s%s", a->type->s ? "__" : "", a->name_lower);
	}

	emit ("</align>);\n");

	emit ("</indent>"
	      "}\n");
    }

    emit ("static gint\n"
	  "%s_write (gsize size, gconstpointer data, gpointer closure)\n"
	  "{\n"
	  "<indent>"
	  "return rcom_port_write_data (RCOM_PORT (closure), size, data);\n"
	  "</indent>"
	  "}\n",
	  p->port_struct->prefix_lower);

    emit ("static struct %s_receive %s_receive_table = {\n"
	  "<indent>",
	  p->prefix_lower,
	  p->prefix_lower);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit (".%s = %s_forward_receive_%s,\n",
	      m->name_lower,
	      p->port_struct->prefix_lower,
	      m->name_lower);
    }

    emit ("</indent>"
	  "};\n");

    emit ("static void\n"
	  "%s_constructed (GObject *gobj)\n"
	  "{\n"
	  "<indent>"
	  "G_OBJECT_CLASS (%s_parent_class)->constructed (gobj);\n"
	  "%s *port = rcom_port_get_port (RCOM_PORT (gobj));\n"
	  "__%s_init (<align>port,\n"
	  "&%s_receive_table,\n"
	  "%s_write,\n"
	  "%s (gobj)</align>);\n"
	  "port->base.context = %s (gobj);\n"
	  "</indent>"
	  "}\n",
	  p->port_struct->prefix_lower,
	  p->port_struct->prefix_lower,
	  p->port_type->stdc,
	  p->port_struct->prefix_lower,
	  p->prefix_lower,
	  p->port_struct->prefix_lower,
	  p->port_struct->prefix_upper,
	  p->port_struct->prefix_upper);

    emit ("static gint\n"
	  "%s_on_write_data (%s *self, gsize size, gconstpointer data)\n"
	  "{\n"
	  "<indent>"
	  "return 0;\n"
	  "</indent>"
	  "}\n",
	  p->port_struct->prefix_lower,
	  p->port_type->glibc);

    emit ("static void\n"
	  "%s_init (%s *self)\n"
	  "{\n"
	  "<indent>"
	  "</indent>"
	  "}\n",
	  p->port_struct->prefix_lower, p->port_type->glibc);

    emit ("static void\n"
	  "%s_class_init (%sClass *cls)\n"
	  "{\n"
	  "<indent>"
	  "GObjectClass *gcls = G_OBJECT_CLASS (cls);\n"
	  "gcls->constructed = %s_constructed;\n"
	  "cls->on_write_data = %s_on_write_data;\n",
	  p->port_struct->prefix_lower, p->port_type->glibc,
	  p->port_struct->prefix_lower,
	  p->port_struct->prefix_lower);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("cls->on_receive_%s = %s_on_receive_%s_default;\n",
	      m->name_lower, p->port_struct->prefix_lower, m->name_lower);
    }

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("/**\n"
	      " * %s::receive-%s\n"
	      " * @self:\n",
	      p->port_type->glibc, m->name);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (" * @%s:",
		  a->name_lower);

	    if (a->length && strcmp (a->length, "@strlen")) {
		emit (" (array length=%s) (element-type %s):",
		      a->length, a->type->glibc);
	    }

	    emit ("\n");
	}

	emit (" */\n");

	emit ("%s_sig_receive_%s =\n"
	      "<indent>"
	      "g_signal_new (<align>\"receive-%s\",\n"
	      "%s,\n"
	      "G_SIGNAL_RUN_FIRST,\n"
	      "G_STRUCT_OFFSET (%sClass, on_receive_%s),\n"
	      "NULL, NULL, NULL,\n"
	      "G_TYPE_NONE, %u",
	      p->port_struct->prefix_lower, m->name_lower,
	      m->name, p->port_type->gtype,
	      p->port_type->glibc, m->name_lower,
	      mk_array_len (m->arg_v));

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n%s",
		  a->length ?
		  g_strcmp0 (a->length, "@strlen") ?
		  "G_TYPE_POINTER" :
		  "G_TYPE_STRING" :
		  a->type->gtype);
	}

	emit ("</align>);\n"
	      "</indent>");
    }

    emit ("</indent>"
	  "}\n");

    emit ("%s *\n"
	  "%s_new ()\n"
	  "{\n"
	  "<indent>"
	  "return BCOM_PORT (rcom_port_construct (<align>%s,\n"
	  "sizeof (%s)</align>));\n"
	  "</indent>"
	  "}\n",
	  p->port_type->glibc, p->port_struct->prefix_lower,
	  p->port_type->gtype, p->port_type->stdc);

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("gint\n"
	      "%s_send_%s (<align>%s *self",
	      p->port_struct->prefix_lower, m->name_lower,
	      p->port_type->glibc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n"
		  "%s %s%s",
		  a->type->glibc,
		  a->length || a->type->s ? "*" : "",
		  a->name_lower);
	}

	emit (",\n"
	      "GError **error</align>)\n"
	      "{\n"
	      "<indent>"
	      "%s *port;\n"
	      "gint ret;\n"
	      "port = rcom_port_get_port (RCOM_PORT (self));\n",
	      p->port_type->stdc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    if (a->type->s) {
		emit ("%s *%s_raw = "
		      "rcom_structure_get_raw (RCOM_STRUCTURE (%s));\n",
		      a->type->stdc, a->name_lower, a->name_lower);
	    }
	}

	emit ("ret = %s_%s (port",
	      p->port_struct->prefix_lower, m->name_lower);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    if (a->type->s) {
		emit (", %s_raw", a->name_lower);
	    } else {
		emit (", %s", a->name_lower);
	    }
	}

	emit (");\n"
	      "if (ret < 0) {\n"
	      "<indent>"
	      "g_set_error (<align>error, RCOM_ERROR, RCOM_ERROR_IO,\n"
	      "\"send %%s failed with code %%d\", \"%s\", ret</align>);\n"
	      "</indent>"
	      "}\n"
	      "return ret;\n"
	      "</indent>"
	      "}\n",
	      m->name);
    }

    MK_ARRAY_FOREACH (p->message_v, Message *, m) {
	emit ("void\n"
	      "%s_emit_receive_%s (<align>%s *self",
	      p->port_struct->prefix_lower,
	      m->name_lower,
	      p->port_type->glibc);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (",\n%s %s%s",
		  a->type->glibc,
		  a->length || a->type->s ? "*" : "",
		  a->name_lower);
	}

	emit ("</align>)\n"
	      "{\n"
	      "<indent>"
	      "g_signal_emit (<align>self, %s_sig_receive_%s,\n0",
	      p->port_struct->prefix_lower, m->name_lower);

	MK_ARRAY_FOREACH (m->arg_v, Arg *, a) {
	    emit (", %s",
		  a->name_lower);
	}

	emit ("</align>);\n");

	emit ("</indent>"
	      "}\n");
    }
}

void
generate_glib (MkString **h_code, MkString **c_code,
	       Protocol *p, Options *o)
{
    generate_h (h_code, p, o);
    generate_c (c_code, p, o);
}
