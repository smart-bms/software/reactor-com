#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define RCOM_TYPE_STRUCTURE (rcom_structure_get_type ())

G_DECLARE_DERIVABLE_TYPE (RComStructure, rcom_structure,
			  RCOM, STRUCTURE, GObject);

struct _RComStructureClass
{
    GObjectClass parent_instance;
};

gpointer    rcom_structure_construct	(GType type,
					 gpointer raw_pointer,
					 gboolean raw_copy,
					 gsize raw_size);
gpointer    rcom_structure_get_raw	(RComStructure *self);
gboolean    rcom_structure_get_raw_copy	(RComStructure *self);
guint	    rcom_structure_get_raw_size	(RComStructure *self);
gboolean    rcom_structure_equal	(RComStructure *self,
					 RComStructure *other);
void	    rcom_structure_copy		(RComStructure *self,
					 RComStructure *dest);
gboolean    rcom_structure_is_reserved	(const gchar *prop_name);

G_END_DECLS
