#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

typedef struct _RComTarget RComTarget;

typedef gint (*RComWrite) (gsize size,
			   gconstpointer data,
			   gpointer closure);
typedef gint (*RComParse) (guint8 code,
			   gsize size,
			   gconstpointer data,
			   gpointer closure);
typedef gint (*RComSerialize) (guint8 code,
			       gsize num_blocks,
			       const gsize *size_v,
			       gconstpointer *data_v,
			       RComWrite write,
			       gpointer write_closure,
			       gpointer closure);
typedef gint (*RComDeserialize) (gsize size,
				 gconstpointer data,
				 RComParse parse,
				 gpointer parse_closure,
				 gpointer closure);

#define RCOM_TYPE_PORT (rcom_port_get_type ())

G_DECLARE_DERIVABLE_TYPE (RComPort, rcom_port,
			  RCOM, PORT, GObject);

struct _RComPortClass
{
    GObjectClass parent_class;

    gint (*on_write_data) (RComPort *self,
			   guint size,
			   gconstpointer data); 
    gint (*serialize) (RComPort *self,
		       guint num_blocks,
		       GBytes **data_v,
		       RComWrite write,
		       gpointer write_closure);
};

RComPort   *rcom_port_construct		(GType type,
					 guint port_size);
void	    rcom_port_set_target	(RComPort *self,
					 RComTarget *target);
RComTarget *rcom_port_get_target	(RComPort *self);
gint	    rcom_port_feed_data		(RComPort *self,
					 guint size,
					 gconstpointer data);
gint	    rcom_port_write_data	(RComPort *self,
					 guint size,
					 gconstpointer data);
gpointer    rcom_port_get_port		(RComPort *self);
void	    rcom_port_set_parse		(RComPort *self,
					 RComParse parse,
					 gpointer parse_closure,
					 GDestroyNotify parse_destroy);
void	    rcom_port_set_serialize	(RComPort *self,
					 RComSerialize serialize,
					 gpointer serialize_closure,
					 GDestroyNotify serialize_destroy);
void	    rcom_port_set_deserialize	(RComPort *self,
					 RComDeserialize deserialize,
					 gpointer deserialize_closure,
					 GDestroyNotify deserialize_destroy);

G_END_DECLS
