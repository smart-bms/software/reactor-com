#include "rcom-target.h"

G_DEFINE_INTERFACE (RComTarget, rcom_target, G_TYPE_OBJECT);

static guint sig_feed;

static gint
write_data (RComTarget *self, guint size, gconstpointer data)
{
    g_error ("RComTarget::write_data");
    return -1;
}

static void
rcom_target_default_init (RComTargetInterface *iface)
{
    iface->write_data = write_data;

    /**
     * RComTarget::feed
     * @self:
     * @size:
     * @data: (array length=size) (element-type guint8):
     */
    sig_feed = g_signal_new ("feed",
			     RCOM_TYPE_TARGET,
			     G_SIGNAL_RUN_FIRST,
			     0, NULL, NULL, NULL,
			     G_TYPE_NONE, 2,
			     G_TYPE_UINT,
			     G_TYPE_POINTER);
}

/**
 * rcom_target_write_data: (virtual write_data)
 * @data: (array length=size) (element-type guint8):
 */
gint
rcom_target_write_data (RComTarget *self, guint size,
			gconstpointer data)
{
    return RCOM_TARGET_GET_IFACE (self)->write_data (self, size, data);
}

/**
 * rcom_target_emit_feed:
 * @data: (array length=size) (element-type guint8):
 */
void
rcom_target_emit_feed (RComTarget *self, guint size,
		       gconstpointer data)
{
    g_signal_emit (self, sig_feed, 0, size, data);
}
