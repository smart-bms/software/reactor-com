#include "bcom-curve.h"

#include <string.h>
#include <stdarg.h>

#define ABS(n) ((n) < 0 ? -(n) : (n))

_Static_assert (sizeof (struct bcom_curve) ==
		sizeof (int16_t) * (BCOM_CURVE_SIZE + 3));

static int16_t
compute (int low_x, int up_x, int count, const int16_t *y_values, int x)
{
    int lower_i, upper_i, lower_x, upper_x;

    lower_i = (count - 1) * (x - low_x) / (up_x - low_x);

    if (lower_i < 0) {
	lower_i = 0;
    } else if (lower_i > (count - 2)) {
	lower_i = count - 2;
    }

    upper_i = lower_i + 1;

    lower_x = (up_x - low_x) * lower_i / (count - 1) + low_x;
    upper_x = (up_x - low_x) * upper_i / (count - 1) + low_x;

    return (y_values[upper_i] - y_values[lower_i]) *
	   (x - lower_x) / (upper_x - lower_x) +
	   y_values[lower_i];
}

static void
remap_values (int low_x, int up_x, int count,
	      int16_t *y_values, int target)
{
    int16_t buf[target];
    int i;

    for (i = 0; i < target; i++) {
	buf[i] = compute (low_x, up_x,
			  count, y_values,
			  i * (up_x - low_x) /
			  (BCOM_CURVE_SIZE - 1) + low_x);
    }

    memcpy (y_values, buf, sizeof (buf));
}

void
bcom_curve_init (struct bcom_curve *curve,
		 int32_t low_x,
		 int32_t up_x,
		 int32_t count,
		 const int32_t *y_values)
{
    int32_t i, x_abs_max, y_abs_max, scale_x, scale_y;
    int16_t y_values_scaled[count];

    x_abs_max = ABS (low_x);

    if (x_abs_max < ABS (up_x)) {
	x_abs_max = ABS (up_x);
    }

    y_abs_max = 0;

    for (i = 0; i < count; i++) {
	if (y_abs_max < ABS (y_values[i])) {
	    y_abs_max = ABS (y_values[i]);
	}
    }

    scale_x = x_abs_max / INT16_MAX + 1;
    scale_y = y_abs_max / INT16_MAX + 1;

    low_x /= scale_x;
    up_x /= scale_x;

    for (i = 0; i < count; i++) {
	y_values_scaled[i] = y_values[i] / scale_y;
    }

    if (count > BCOM_CURVE_SIZE) {
	remap_values (low_x, up_x, count, y_values_scaled, BCOM_CURVE_SIZE);

	count = BCOM_CURVE_SIZE;
    }

    curve->scale_x = scale_x;
    curve->scale_y = scale_y;
    curve->count = count;
    curve->low_x = low_x;
    curve->up_x = up_x;

    memcpy (curve->y_values, y_values_scaled, count * sizeof (int16_t));
}

void
bcom_curve_init_v (struct bcom_curve *curve,
		   int32_t low_x,
		   int32_t up_x,
		   int32_t count,
		   int32_t y_first, ...)
{
    int32_t y_values[count];
    int32_t i;
    va_list args;

    y_values[0] = y_first;
    va_start (args, y_first);

    for (i = 1; i < count; i++) {
	y_values[i] = va_arg (args, int);
    }

    va_end (args);

    bcom_curve_init (curve, low_x, up_x, count, y_values);
}

int32_t
bcom_curve_compute (const struct bcom_curve *curve, int32_t x)
{
    return compute (curve->low_x, curve->up_x, curve->count,
		    curve->y_values, x / curve->scale_x) * curve->scale_y;
}
