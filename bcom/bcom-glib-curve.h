#pragma once

#include <rcom-glib.h>
#include <bcom-curve.h>

G_BEGIN_DECLS

#define BCOM_TYPE_CURVE (bcom_curve_get_type ())

G_DECLARE_FINAL_TYPE (BComCurve, bcom_curve,
		      BCOM, CURVE, RComStructure);

BComCurve  *bcom_curve_new	(void);
BComCurve  *bcom_curve_new_wrap	(struct bcom_curve *raw,
				 gboolean copy);

G_END_DECLS
