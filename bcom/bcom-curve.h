#pragma once

#include <stdint.h>

#define BCOM_CURVE_SIZE 9

struct bcom_curve
{
    uint16_t scale_x:6;
    uint16_t scale_y:6;
    uint16_t count:4;
    int16_t low_x;
    int16_t up_x;
    int16_t y_values[BCOM_CURVE_SIZE];
};

void bcom_curve_init (struct bcom_curve *curve,
		      int32_t low_x,
		      int32_t up_x,
		      int32_t count,
		      const int32_t *y_values);
void bcom_curve_init_v (struct bcom_curve *curve,
			int32_t low_x,
			int32_t up_x,
			int32_t count,
			int32_t y_first, ...);
int32_t bcom_curve_compute (const struct bcom_curve *curve,
			    int32_t x);
