#pragma once

#include <glib.h>

G_BEGIN_DECLS

#define RCOM_ERROR (rcom_error_quark ())

typedef enum {
    RCOM_ERROR_IO,
} RComError;

GQuark	rcom_error_quark    (void) G_GNUC_CONST;

G_END_DECLS
