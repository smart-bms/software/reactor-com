#include "rcom-termios.h"
#include "rcom-error.h"

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <asm/termios.h>

extern int ioctl (int, unsigned long, ...);

typedef struct {
    guint baudrate;
} RComTermiosPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (RComTermios, rcom_termios, RCOM_TYPE_FILE);

enum
{
    PROP_0,
    PROP_DEVICE,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void
set_property (GObject *gobj, guint id, const GValue *value, GParamSpec *spec)
{
    RComTermios *self = RCOM_TERMIOS (gobj);

    switch (id) {
    case PROP_DEVICE:
	rcom_termios_set_device (self, g_value_get_string (value));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
get_property (GObject *gobj, guint id, GValue *value, GParamSpec *spec)
{
    RComTermios *self = RCOM_TERMIOS (gobj);

    switch (id) {
    case PROP_DEVICE:
	g_value_set_string (value, rcom_termios_get_device (self));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, id, spec);
    }
}

static void
on_outpath_changed (GObject *gobj)
{
    g_object_notify_by_pspec (gobj, props[PROP_DEVICE]);
}

static void
constructed (GObject *gobj)
{
    G_OBJECT_CLASS (rcom_termios_parent_class)->constructed (gobj);

    g_signal_connect (gobj, "notify::outpath",
		      G_CALLBACK (on_outpath_changed), NULL);

    g_object_bind_property (gobj, "outpath",
			    gobj, "inpath",
			    G_BINDING_SYNC_CREATE);
}

static void
mkinput (RComFile *base, GFile *file, GError **error)
{
    g_error ("RComTermios::mkinput");
}

static void
mkoutput (RComFile *base, GFile *file, GError **error)
{
    g_autofree gchar *path = NULL;
    RComTermios *self = RCOM_TERMIOS (base);
    RComTermiosPrivate *p = rcom_termios_get_instance_private (self);
    struct termios2 conf;
    gint fd;

    path = g_file_get_path (file);

    if (!rcom_file_is_same_io (base)) {
	g_set_error (error, RCOM_ERROR, RCOM_ERROR_IO,
		     "different input and output files");
	return;
    }

    fd = open (path, O_RDWR | O_NOCTTY | O_NONBLOCK);

    if (fd < 0) {
	g_set_error (error, RCOM_ERROR, RCOM_ERROR_IO,
		     "cannot open file: %s",
		     rcom_termios_get_device (self));
	return;
    }

    if (!isatty (fd)) {
	g_set_error (error, RCOM_ERROR, RCOM_ERROR_IO,
		     "file %s is not a tty device",
		     rcom_termios_get_device (self));
	close (fd);
	return;
    }

    memset (&conf, 0, sizeof (conf));

    conf.c_ispeed = p->baudrate;
    conf.c_ospeed = p->baudrate;
    conf.c_cflag |= BOTHER | CS8 | CREAD | CLOCAL;
    conf.c_cc[VMIN] = 0;
    conf.c_cc[VTIME] = 10;

    if (ioctl (fd, TCSETS2, &conf) < 0) {
	close (fd);
	g_set_error (error,
		     RCOM_ERROR,
		     RCOM_ERROR_IO,
		     "cannot configure tty device: %s",
		     rcom_termios_get_device (self));
	return;
    }

    close (fd);
}

static void
rcom_termios_init (RComTermios *self)
{
    RComTermiosPrivate *p = rcom_termios_get_instance_private (self);

    p->baudrate = 115200;
}

static void
rcom_termios_class_init (RComTermiosClass *cls)
{
    RComFileClass *fcls = RCOM_FILE_CLASS (cls);
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    fcls->mkinput = mkinput;
    fcls->mkoutput = mkoutput;
    gcls->set_property = set_property;
    gcls->get_property = get_property;
    gcls->constructed = constructed;

    props[PROP_DEVICE] = g_param_spec_string ("device",
					      "device",
					      "device",
					      NULL,
					      G_PARAM_READWRITE |
					      G_PARAM_CONSTRUCT |
					      G_PARAM_STATIC_STRINGS |
					      G_PARAM_EXPLICIT_NOTIFY);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

RComTermios *
rcom_termios_new (const gchar *device)
{
    return g_object_new (RCOM_TYPE_TERMIOS, "device", device, NULL);
}

void
rcom_termios_set_device (RComTermios *self, const gchar *device)
{
    rcom_file_set_outpath (RCOM_FILE (self), device);
}

const gchar *
rcom_termios_get_device (RComTermios *self)
{
    return rcom_file_get_outpath (RCOM_FILE (self));
}
